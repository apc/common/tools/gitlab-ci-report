
const
  { describe, it, after } = require('mocha'),
  { expect } = require('chai'),
  sa = require('superagent');

var app = require('../src'); /* will start the server */

describe('Server tests', function() {

  var srv;
  before(function(done) {
    /* start the server */
    /* note: here we use 'done' callback to tell that before step is finished */
    srv = app.listen(1234, 'localhost', () => done());
  });

  after(function() {
    /* note: always clean the env after your tests */
    srv.close();
  });


  it('can serve requests', function() {
    /* note: here we use the 'promise' mode where a promise is returned by the
       function, the test will end when the promise resolves, and will fail
       if the promise is rejected */
    return sa.get('localhost:1234')
    .then((ret) => {
      /* note: prefer wrapping in should rahter than calling ret.should.containEql,
         this prevent failure on undefined ret values.

         Use as many assertions as required. */
      expect(ret).to.contain({ status: 200, type: 'text/html' });
      expect(ret).to.have.property('text').contain('router-view');
    });
  });
});
