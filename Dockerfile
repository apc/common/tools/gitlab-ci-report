FROM node:lts-slim

WORKDIR /app
ADD . /app
RUN apt-get update && \
    apt-get install git --yes && \
    npm install --production && \
    npm install pm2 && mkdir .pm2 && chmod 777 .pm2 && \
    cd www && npm install --production && \
    apt-get clean
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:node_modules/.bin
ENV HOME=/app

CMD pm2-docker -n app --no-autorestart ./src/index.js
EXPOSE 8080/tcp
