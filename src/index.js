const express = require("express");
const { get, attempt } = require("lodash");
const path = require("path");
const qs = require("querystring");
const axios = require("axios");
const serveStatic = require("express-static-gzip");

const GITLAB_SRC = "https://gitlab.cern.ch/api/v4";
const BASE_NAMESPACE = "mro"; // Base namespace
const DEFAULT_PER_PAGE = 40; // Max number of element per page
const DEFAULT_PAGE = 1; // default page index

var config;

try {
  /* $FlowIgnore */
  config = require("/etc/app/config"); /* eslint-disable-line global-require */
} catch (e) {
  config = { basePath: "" };
}

const gitlabAxios = axios.create();
if (config?.token || process.env["AUTH_TOKEN"]) {
  gitlabAxios.defaults.headers['Authorization'] = `Bearer ${config.token ?? process.env["AUTH_TOKEN"]}`;
}

var app = express();
var router = express.Router();

// FRONT EXPOSITION
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

const dist = path.join(__dirname, "..", "www", "dist");
router.use("/dist", serveStatic(dist, { enableBrotli: true }));

router.get("/", (req, res) => res.render("index", config));

// GITLAB API EXPOSITION

function extract_namespace(req) {
  return qs.escape(get(req, "query.namespace", BASE_NAMESPACE));
}

function extract_page(req) {
  return qs.escape(get(req, "query.page", DEFAULT_PAGE));
}

function extract_per_page(req) {
  return qs.escape(get(req, "query.per_page", DEFAULT_PER_PAGE));
}

/** @param {axios.AxiosResponse} ret */
function handle_headers(ret) {
  return {
    page: ret.headers["x-page"],
    next_page: ret.headers["x-next-page"],
    prev_page: ret.headers["x-prev-page"],
    per_page: ret.headers["x-per-page"],
    total: ret.headers["x-total"],
    total_pages: ret.headers["x-total-pages"],
  };
}

/* default namespace route */
router.get("/namespace", (req, res) =>
  res.json(config.namespace || BASE_NAMESPACE)
);

/* all subgroups for a group */
router.get("/groups", (req, res) => {
  const namespace = extract_namespace(req);
  const id_page = extract_page(req);
  const per_page = extract_per_page(req);

  return gitlabAxios.get(`${GITLAB_SRC}/groups/${namespace}/subgroups`, {
      params: { simple: 1, page: id_page, per_page: per_page }
  })
  .then(
    (ret) => {
      res.set(handle_headers(ret));
      res.json(ret.data);
    },
    (err) => res.status(500).send(err));
});

/* all projects in a group */
router.get("/projects", (req, res) => {
  const namespace = extract_namespace(req);
  const id_page = extract_page(req);
  const per_page = extract_per_page(req);

  return gitlabAxios.get(`${GITLAB_SRC}/groups/${namespace}/projects`, {
      params : { simple: 1, page: id_page, per_page: per_page }
  })
  .then(
    (ret) => {
      res.set(handle_headers(ret));
      res.json(ret.data);
    },
    (err) => res.status(500).send(err));
});

/* all badges for a project */
router.get("/project/:id/badges", (req, res) => {
  const id_page = extract_page(req);
  const per_page = extract_per_page(req);

  return gitlabAxios.get(`${GITLAB_SRC}/projects/${req.params.id}/badges`,
      { params: { page: id_page, per_page: per_page }
  })
  .then(
    (ret) => {
      res.set(handle_headers(ret));
      res.json(ret.data);
    },
    (err) => res.status(500).send(err));
});

app.use(config.basePath, router);

if (!module.parent) {
  /* we're called as a main, let's listen */
  app.listen(process.env.PORT || 8080, () =>
    console.log("Server listening on port " + (process.env.PORT || 8080))
  );
} else {
  /* export our server */
  module.exports = app;
}
