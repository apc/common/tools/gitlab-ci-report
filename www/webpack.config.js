var path = require('path')
var webpack = require('webpack')
const _ = require('lodash');
const
  { VueLoaderPlugin } = require('vue-loader'),
  CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: 'main.js',
    hashFunction: 'xxhash64'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [ 'vue-style-loader', 'css-loader' ],
      },      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.pug$/,
        loader: 'pug-plain-loader',
        oneOf: [
          // this applies to `<template lang="pug">` in Vue components
          {
            resourceQuery: /^\?vue/,
            use: ['pug-plain-loader']
          }
        ]
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          extends: path.join(__dirname, '.babelrc')
        }
      },
      {
        test: /\.scss$/,
        use: [ 'vue-style-loader', 'css-loader', 'sass-loader' ]
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      'TweenMax': 'gsap/TweenMax'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: "warning"
  },
  devtool: 'eval-cheap-source-map',//inline-source-map',
  plugins: [ new VueLoaderPlugin() ]
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = 'source-map'
  module.exports.mode = 'production'
  _.set(module.exports, 'optimization.nodeEnv', 'production');
  _.set(module.exports, 'optimization.moduleIds', 'named');

  module.exports.plugins.push(
    new CompressionPlugin({ algorithm: 'gzip', threshold: 10240, minRatio: 0.8 }),
    new CompressionPlugin({ algorithm: 'brotliCompress', filename: "[path][base].br", threshold: 10240, minRatio: 0.8 }));
}
