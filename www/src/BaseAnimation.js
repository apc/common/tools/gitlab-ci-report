
import _ from 'lodash'
import { select } from 'd3-selection';
import { easeBounceOut, easeExpOut } from 'd3-ease';
import { interpolateNumber, interpolateString } from 'd3-interpolate';
import 'd3-transition'; // load the plugin
import { active } from 'd3-transition';

/* get the very last argument as a callback, manages an optional parameters */
function getCallback(args) {
  var done = _.last(args);
  return _.isFunction(done) ? done : _.noop;
}

function getStyle(elt) {
  return window.getComputedStyle(elt, null);
}

function getDom(elt) {
  if (_.has(elt, 'elm')) {
    /* VNode */
    return elt.elm;
  }
  else if (_.has(elt, '$el')) {
    /* VueComponent */
    return elt.$el;
  }
  return elt;
}

function getRandom(max, min){
	return Math.floor(Math.random() * (1 + max - min) + min);
}

function getAnim(selection, name, options) {
  let trans = selection.property('anim-' + name);
  if (trans && active(selection.node(), name)) {
    if (_.get(options, 'cancel', false)) {
      selection.interrupt(name);
      return selection.transition(name);
    }
    return trans.transition();
  }
  return selection.transition(name);
}

function setAnim(selection, name, transition) {
  selection.property('anim-' + name, transition);
  return transition;
}

function defaultStyle(style, defaultValue) {
  return (!style || style === 'none') ? defaultValue : style;
}

function styleFactory(name, from, to, interpolator = interpolateNumber) {
  return function() {
    return interpolator(
      defaultStyle(select(this).style(name), from), to);
  }
}

var BaseAnimation = {
  animationSpeed: 1,
  'setOpacity': function(elt, opacity) { /* not an animation, just an helper */
    gsap.then((gsap) => {
      elt = getDom(elt);
      gsap.TweenMax.set(elt, { opacity });
    });
  },
  'getDom': getDom,
  'randomTranslateIn': function(elt, options, done) {
    done = getCallback(arguments);
    const name = _.get(options, 'name', 'base-anim');
    const sel = select(getDom(elt));

    setAnim(sel, name,
      getAnim(sel, name, options)
      .call((transition, x, y) => {
        transition.selection()
        .style('position', 'absolute')
        .style('x', getRandom(300, -300))
        .style('y', getRandom(300, -300));
      })
      .transition()
      .call(() => console.log('called'))
      .style('x', 0)
      .style('y', 0)
      .duration(1200 * BaseAnimation.animationSpeed)
      .ease(easeBounceOut)
    )
    .end().then(() => done(), () => done());
  },
  'flipIn': function(elt, options, done) {
    done = getCallback(arguments);
    const name = _.get(options, 'name', 'base-anim');
    const sel = select(getDom(elt));

    setAnim(sel, name,
      getAnim(sel, name, options)
      .duration(1200 * BaseAnimation.animationSpeed)
      .ease(easeBounceOut)
      .styleTween('opacity', styleFactory('opacity', 0, 1))
      .styleTween('transform', styleFactory(
        'transform', 'rotateX(-90deg)', 'rotateX(0deg)', interpolateString))
    )
    .end().then(() => done(), () => done());
  },
  'flipOut': function(elt, options, done) {
    done = getCallback(arguments);
    const name = _.get(options, 'name', 'base-anim');
    const sel = select(getDom(elt));

    setAnim(sel, name,
      getAnim(sel, name, options)
      .duration(1200 * BaseAnimation.animationSpeed)
      .ease(easeBounceOut)
      .styleTween('opacity', styleFactory('opacity', 1, 0))
      .styleTween('transform', styleFactory(
        'transform', 'rotateX(0deg)', 'rotateX(-90deg)', interpolateString))
    )
    .end().then(() => done(), () => done());
  },
  'fadeIn': function(elt, options, done) {
    done = getCallback(arguments);
    const name = _.get(options, 'name', 'base-anim');
    const sel = select(getDom(elt));

    setAnim(sel, name,
      getAnim(sel, name, options)
      .duration(300 * BaseAnimation.animationSpeed)
      .styleTween('opacity', styleFactory('opacity', 0, 1))
      .ease(easeExpOut)
    )
    .end().then(() => done(), () => done());
  },
  'fadeOut': function(elt, options, done) {
    done = getCallback(arguments);
    const name = _.get(options, 'name', 'base-anim');
    const sel = select(getDom(elt));

    setAnim(sel, name,
      getAnim(sel, name, options)
      .duration(300 * BaseAnimation.animationSpeed)
      .styleTween('opacity', styleFactory('opacity', 1, 0))
      .ease(easeExpOut)
    )
    .end().then(() => done(), () => done());
  }
};

_.assign(BaseAnimation, {
  defaultIn: BaseAnimation.flipIn,
  defaultOut: BaseAnimation.fadeOut
});

export default BaseAnimation;
