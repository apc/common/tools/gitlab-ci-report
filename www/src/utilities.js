//@flow
import _ from 'lodash';
import axios from 'axios';

var _currentUrl = window.location.origin + window.location.pathname;

export function currentUrl() {
  return _currentUrl;
}

export function setCurrentUrl(url /*: any */) {
  _currentUrl = url;
}

/**
* Iteratively fetch all pages from the request.
* @param {String} url the url for the request.
* @param {Object} params the parameters given to the call.
* @param {Object} opts the interruption handler (will stop if `opts.aborted`).
* @param {Function} updateFunction the async function to add the fetched to the buffer as goind. Must take one parameter for the returned request data.
* @return {void} 
*/
export async function fetchAllPages(url, params, opts, updateFunction) {
  let next_page = 1; // Initialize the loop at the first page

  // Continue to fetch until it's the last page
  while (next_page) {
    
    const response = await axios.get(url, { params: { ...params, page: next_page } });
    
    if (_.get(opts, 'aborted')) { return; } // Interupt if aborted

    await updateFunction(response.data); // Update datas as going
    
    next_page = response.headers.next_page;
  }
}