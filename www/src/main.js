
import './public-path';

import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
const rateLimit = import(/* webpackChunkName: "axios-rate-limit" */ 'axios-rate-limit');

rateLimit.then((rl) => {
  rl.default(axios, { maxRequests: 2, perMilliseconds: 200 });
});

Vue.use(VueRouter);

import App from './App.vue';

const router = new VueRouter({
  routes: [
    { path: '/', component: App,
      props: (route) => ({
        recursive: route.query.recursive,
        namespace: route.query.namespace })
    }
  ]
});

new Vue({
  router: router,
  el: '#app'
});
