
/* for webpack module loaders */
import { currentUrl } from './utilities';

// eslint-disable-next-line
if (typeof __webpack_public_path__ !== 'undefined') {
  // eslint-disable-next-line
  __webpack_public_path__ = currentUrl() + '/dist/';
}
