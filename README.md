# CI-Report

This is a simple webite made with [pug](https://pugjs.org/) and [vue3](https://vuejs.org/) to display the status for every project in the gitlab group

## 1. Getting started

### 1.1. Setup

1. Clone this project with `git clone https://gitlab.cern.ch/mro/common/tools/gitlab-ci-report.git`
2. Install [nodejs](https://nodejs.org/en/) (`v12+` recommanded)
3. Setup with `npm run install`

### 1.2. Run

```bash
npm run build   # build and reload the dist folder
```

## 2. Code

### 2.1. Front

The `App.vue` component will recursively call the api to fetch all groups (if the `recursive` prop is false, it will omly get the projects in the current group).

For each group, it will fetch and sort the belonging projects and instanciate for each one a `Project.vue` component as a row of the table.

Each `Project.vue` will then retrieve it's badges from the api.

---

If a request does not return all the available datas because of page limitation (see `per_page` for the number of elements per page), it will iteratively keep calling the same request with the next page to be fetched, until the end of the datas is reached.

### 2.2. Back

Gateway exposing the resources of the gitlab api (from `https://gitlab.cern.ch/api/v4/`).

| URL | GITLAB | RETURNED |
| --- | --- | --- |
| `/namespace` | `mro/common` *(or config namespace)* | base group  |

For each of the following request, some parameters can be specified:

- `namespace` to target a specific group / subgroup
- `page` to get a specific page (default in `DEFAULT_PAGE`)
- `per_page` to specify the number of items per page (default in `DEFAULT_PER_PAGE`)

| URL | GITLAB | RETURNED |
| --- | --- | --- |
| `/groups` | `/groups/${namespace}/subgroups` | all subgroups for a group |
| `/projects` | `/groups/${namespace}/projects` | all projects in a group |
| `/project/:id/badges` | `/projects/${id}/badges` | all badges for a project |

